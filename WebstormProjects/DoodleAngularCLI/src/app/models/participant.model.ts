import {MeetingParticipation} from "./meetingparticipation.model";
/**
 * Created by Delt on 20/02/2018.
 */
export class Participant{

  constructor(public name: string, public personalmail: string, public personID?: number ){
  }
}
