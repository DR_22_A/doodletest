import {MeetingPlannerComponent} from "../meetingPlanner/meeting-planner.component";
import {MeetingParticipation} from "./meetingparticipation.model";
/**
 * Created by Delt on 20/02/2018.
 */

export class MeetingTimes{
  constructor(public meeting: any, public startDate: any,  public duration: number,public ID: any, public dateLabel?: any, public meetingparticipation?: MeetingParticipation[], public timeStamps?: any[]){}
}
