import {MeetingTimes} from "./meetingtimes.model";
/**
 * Created by Delt on 20/02/2018.
 */

export class Meeting{
    constructor(public title: string, public visible: boolean, public meeting: any, public meetingRoom: any, public startDate?: any, public endDate?: any, public description?: string, public meetingTimes?: MeetingTimes[]){}
}
