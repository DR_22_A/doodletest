import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {MeetingPlannerComponent} from "./meetingPlanner/meeting-planner.component";
import {MeetingPlannerModule} from "./meetingPlanner/meeting-planner.module";
import {Routes, RouterModule} from "@angular/router";
import {MeetingService} from "./services/meeting.service";
import {HttpClientModule} from "@angular/common/http";
import {MyDatePickerModule} from "mydatepicker";
import {  } from '@angular/material';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatButtonModule} from "@angular/material";
import {MaterialModule} from "./material.module";
import {MeetingParticipationService} from "./services/meetingparticipation.service";
import {MeetingTimesService} from "./services/meetingtimes.service";
import {ParticipantService} from "./services/participant.service";
import {routing} from "./app.route";
import {AmazingTimePickerModule} from "amazing-time-picker";
const APP_ROUTES: Routes = [
  {path: 'planner', component:MeetingPlannerComponent},
  {path: 'planner/:id', component: MeetingPlannerComponent}
];
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    MeetingPlannerModule,
    routing,
    MyDatePickerModule,AmazingTimePickerModule,
    BrowserAnimationsModule,
    MaterialModule

  ],
  providers: [ MeetingService, MeetingTimesService, MeetingParticipationService, ParticipantService],
  bootstrap: [AppComponent]
})
export class AppModule { }
