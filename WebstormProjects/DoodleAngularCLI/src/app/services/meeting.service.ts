import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {HttpModule} from '@angular/http';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {Meeting} from "../models/meeting.model";
/**
 * Created by Delt on 20/02/2018.
 */

@Injectable()
export class MeetingService {
    constructor (private http: HttpClient){}

    private readonly rootURL = 'http://lab02.ometa.net:50557/odata/runtime/Meetings/';

    getSingleMeeting(id: string): Observable<any>{
      const url = "http://lab02.ometa.net:50557/odata/runtime/Meetings/MeetingPlanner?$filter=Number eq '" + id +  "'";
      return this.http.get<any>(url);
    }
    addToMeetingTimes(){

    }
}
