/**
 * Created by Delt on 23/02/2018.
 */
import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import {HttpModule} from '@angular/http';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {Meeting} from "../models/meeting.model";
import {MeetingTimes} from "../models/meetingtimes.model";
import {HttpHeaders} from "@angular/common/http";
/**
 * Created by Delt on 20/02/2018.
 */

@Injectable()
export class MeetingTimesService {
  constructor (private http: HttpClient){}

  getSingleMeetingTimes(id: string): Observable<any>{
    const url = "http://lab02.ometa.net:50557/odata/runtime/Meetings/MeetingPlanner?$filter=Number eq '" + id +  "'";
    return this.http.get<any>(url);
  }
  GetMeetingTimesPerMeeting(meetingID: any): Observable<any>{
    console.log(meetingID);
    const url = "http://lab02.ometa.net:50557/odata/runtime/Meetings/MeetingTimes?$filter=MeetingID eq " + meetingID+ "";
    return this.http.get<any>(url);
  }

  addToMeetingTimes(meetingTime: MeetingTimes){

    //JSON Object aanmaken. Gegevens komen overeen met de velden die als Input gebruikt worden in framework.
    let jsonObject = {
      "MeetingID": meetingTime.meeting.meeting,
      "startDate": meetingTime.startDate,
    };
    const body = JSON.stringify(jsonObject); //Omzetten naar string voor versturen
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'}); //

    return this.http.post("http://lab02.ometa.net:50557/odata/runtime/Meetings/MeetingTimes", body, {headers: headers}); //Posten naar de URL.
  }
}
