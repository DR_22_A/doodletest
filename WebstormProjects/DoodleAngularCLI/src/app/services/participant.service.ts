import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
/**
 * Created by Delt on 24/02/2018.
 */
@Injectable()
export class ParticipantService{

  constructor (private http: HttpClient){}

  getExternalParticipantMailFromID(id: any){
    const url = "http://lab02.ometa.net:50557/odata/runtime/ExternalPeople/MeetingParticipation("+ id +")";
    return this.http.get<any>(url);
  }
  getExternalParticipantFromEmail(email: string) : Observable<any>{
    const url = "http://lab02.ometa.net:50557/odata/runtime/ExternalPeople/Persons?$filter=EmailDB eq '" + email +  "'";
    return this.http.get<any>(url);
  }

  getInternalParticipantFromEmail(email: string): Observable<any>{
    const url = "http://lab02.ometa.net:50557/odata/runtime/People/Persons?$filter=Email eq '" + email +  "'";
    return this.http.get<any>(url);
  }
}
