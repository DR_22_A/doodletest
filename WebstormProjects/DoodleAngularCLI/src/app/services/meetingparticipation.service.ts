/**
 * Created by Delt on 23/02/2018.
 */
import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {HttpModule} from '@angular/http';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {Meeting} from "../models/meeting.model";
import {Participant} from "../models/participant.model";
import {HttpHeaders} from "@angular/common/http";
import {isNullOrUndefined} from "util";
/**
 * Created by Delt on 20/02/2018.
 */

@Injectable()
export class MeetingParticipationService {
  constructor (private http: HttpClient){}

  getSingleMeetingTimes(id: string): Observable<any>{
    const url = "http://lab02.ometa.net:50557/odata/runtime/Meetings/Meetings?$filter=Number eq '" + id +  "'";
    return this.http.get<any>(url);
  }
  GetParticipationAtTime(meetingTime: any){
    const url = "http://lab02.ometa.net:50557/odata/runtime/Meetings/MeetingParticipation?$filter=MeetingTimeID eq " + meetingTime+ "";
    return this.http.get<any>(url);
  }
  GetParticipationsForMeeting(meeting: any){

  }

  checkParticipationExistance(meetingTimeID: any, personID: any, email: any, status: string){
    //Check if person meeting exists
    const url = "http://lab02.ometa.net:50557/odata/runtime/Meetings/MeetingParticipation";


    if (personID != 0) {
      this.http.get("http://lab02.ometa.net:50557/odata/runtime/Meetings/MeetingParticipation?$filter=MeetingTimeID eq " + meetingTimeID + " and PersonID eq " + personID)
        .subscribe(odata => {
          let data = odata['value'];
          let jsonObject = {
            "MeetingTimeID": meetingTimeID,
            "PersonID": personID,
            "Availability": status
          };
          if (isNullOrUndefined(data[0])){

            this.addToParticipations(jsonObject);
          }
          else{
            this.editParticipations(data[0].ID, jsonObject);
          }

        })
    }
    else{
      this.http.get("http://lab02.ometa.net:50557/odata/runtime/Meetings/MeetingParticipation?$filter=MeetingTimeID eq " + meetingTimeID + " and AdID eq '" + email+"'")
        .subscribe(odata=>{
          let data = odata['value'];
          let jsonObject = {
            "MeetingTimeID": meetingTimeID,
            "Availability": status,
            "AdID": email
          };
          if (isNullOrUndefined(data[0])) {
            this.addToParticipations(jsonObject);
          }
          else{
            this.editParticipations(data[0].ID, jsonObject);
          }
        })
    }
  }
  addToParticipations(jsonObject: any){
    const body = JSON.stringify(jsonObject); //Omzetten naar string voor versturen
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'}); //

    console.log(body);
    return this.http.post("http://lab02.ometa.net:50557/odata/runtime/Meetings/MeetingParticipation", body, {headers: headers});
  }
  //In case person is an external one

  editParticipations(id: any, jsonObject: any){
    const body = JSON.stringify(jsonObject); //Omzetten naar string voor versturen
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'}); //

    this.http.put("http://lab02.ometa.net:50557/odata/runtime/Meetings/MeetingParticipation("+ id +")", body , {headers: headers});
  }

}
