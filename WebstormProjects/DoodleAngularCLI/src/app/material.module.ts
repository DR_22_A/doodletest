/**
 * Created by Delt on 22/02/2018.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule, MatTableModule, MatFormFieldModule, MatInputModule} from '@angular/material';

@NgModule({
  imports: [MatButtonModule, MatTableModule, MatFormFieldModule, MatInputModule],
  exports: [MatButtonModule, MatTableModule, MatFormFieldModule, MatInputModule],
})
export class MaterialModule { }
