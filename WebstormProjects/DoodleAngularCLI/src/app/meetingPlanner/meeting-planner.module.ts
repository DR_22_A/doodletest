/**
 * Created by Delt on 19/02/2018.
 */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MeetingPlannerComponent} from "./meeting-planner.component";
import {MyDatePickerModule} from "mydatepicker";
import {FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "../material.module";

@NgModule({
    declarations: [
        MeetingPlannerComponent
    ],
    imports: [BrowserModule, MyDatePickerModule, FormsModule, ReactiveFormsModule, MaterialModule]
})
export class MeetingPlannerModule {

}
