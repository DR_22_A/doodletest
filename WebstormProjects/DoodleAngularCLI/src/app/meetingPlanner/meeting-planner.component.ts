/**
 * Created by Delt on 19/02/2018.
 */
import { Component } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Meeting} from "../models/meeting.model";
import {MeetingService} from "../services/meeting.service";
import {MeetingTimes} from "../models/meetingtimes.model";
import {IMyDpOptions} from "mydatepicker";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Participant} from "../models/participant.model";
import {MeetingParticipation} from "../models/meetingparticipation.model";
import {MeetingTimesService} from "../services/meetingtimes.service";
import {MeetingParticipationService} from "../services/meetingparticipation.service";
import {ParticipantService} from "../services/participant.service";
import {Response} from "@angular/http";
import {AmazingTimePickerService} from 'amazing-time-picker';

@Component({
    selector: 'meeting-planner',
    templateUrl: './meeting-planner.component.html'
})
export class MeetingPlannerComponent {

    id:any = "test";
    myOwnEmail: any = "";
    private sub: any;
    meeting: Meeting;
    meetingTimes: MeetingTimes[] = [];
    participants: Participant[] = [];
    meetingParticipants: MeetingParticipation[] = [];
    selectedTime: any = "00:00";

    participantEmails: string[] = [];

    constructor(private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private meetingService: MeetingService,
                private meetingTimeService: MeetingTimesService,
                private meetingParticipationsService: MeetingParticipationService,
                private participantService: ParticipantService,
                private atp: AmazingTimePickerService){}

    ngOnInit(){
        this.sub = this.route.params.subscribe(params =>{
            if (params != null){
                this.id = params['id'];
            }
        });
      this.sub = this.route.params.subscribe(params =>{
        if (params != null){
          this.myOwnEmail = params['email'];
          if (this.myOwnEmail != null){
            this.GetPersonByMail(this.myOwnEmail, true);
          }
        }
      });

      this.sub = this.route.params.subscribe(params =>{
        if (params != null){
          this.id = params['id'];
        }
      });
        if (this.id){
          this.meetingService.getSingleMeeting(this.id)
            .subscribe(data =>{
              let odata = data.value[0];

              let newMeeting: Meeting = new Meeting(odata.Title, odata.Visible, odata.MeetingID, odata.MeetingRoomID, odata.Start, odata.End, odata.Description);
              this.meeting = newMeeting;
              this.getMeetingTimes();
            })
        }
      this.myForm = this.formBuilder.group({
        myDate: [null, Validators.required]
      });
    }
    open(){
      const amazingTimePicker = this.atp.open();
      amazingTimePicker.afterClose().subscribe(time => {
        this.selectedTime = time;
      });
    }
    getMeetingTimes(){
      this.meetingTimes = [];
      this.meetingParticipants = [];

      this.meetingTimeService.GetMeetingTimesPerMeeting(this.meeting.meeting)
        .subscribe(data =>{
          for (let meetingTime of data.value){
            let date = new Date(meetingTime.startDate);
            this.meetingTimes.push(new MeetingTimes(meetingTime.MeetingID, date, 1, meetingTime.ID ,date.toLocaleDateString() ));
            this.getMeetingParticipations(meetingTime.ID);
          }

          this.getMeetingParticipants();
        });
    }

    getMeetingParticipations(meetingTimeId: any){
      this.meetingParticipationsService.GetParticipationAtTime(meetingTimeId)
        .subscribe(odata=>{
          let data = odata['value'];
          for (let datapoints of data){
            let meetingParticip = new MeetingParticipation(datapoints.MeetingTimeID, datapoints.PersonID, datapoints.Availability, datapoints.AdID);
            this.meetingParticipants.push(meetingParticip);
          }
        })
    }
    getMeetingParticipants(){
      let idArray = [];
      let adArray = [];
      for(let meetingTime of this.meetingTimes){
        this.meetingParticipationsService.GetParticipationAtTime(meetingTime.ID)
          .subscribe(odata =>{
            let data = odata['value']
            for (var i = 0; i < data.length; i++){
              if (data[i].PersonID == 0){
                if (adArray.indexOf(data[i].AdID) == -1){
                  adArray.push(data[i].AdID)
                }
              }
              else{
                if (idArray.indexOf(data[i].PersonID) == -1){
                  idArray.push(data[i].PersonID)
                }
              }
            }
            this.GetPeopleAndAddToParticipants(idArray, adArray);
          });
      }
    }
    GetPeopleAndAddToParticipants(idArray: any, adArray: any){
      for (var i = 0; i < idArray.length; i++){
        this.participantService.getExternalParticipantMailFromID(idArray[i])
          .subscribe(odata=>{
            this.GetPersonByMail(odata["Email"], true);
          })
      }
      for(var i = 0; i < adArray.length; i++){
        this.GetPersonByMail(adArray[i], true);
      }
    }

    GetAttendanceStatus(personId: any, meetingTimeID: any, email: any)
    {
        var returnVal = "Misschien";
        if (personId != 0){
          for (var i = 0; i < this.meetingParticipants.length; i++){
            if (this.meetingParticipants[i].participant == personId && this.meetingParticipants[i].meetingtimes == meetingTimeID){
              returnVal =  (this.meetingParticipants[i].availability)
            }
          }
        }
        else{
          for (var i = 0; i < this.meetingParticipants.length; i++){
            if (this.meetingParticipants[i].email == email && this.meetingParticipants[i].meetingtimes == meetingTimeID){
              returnVal =  (this.meetingParticipants[i].availability)
            }
          }
        }
        return returnVal;
    }

    AddToMeetingTimes(time){
        var splittedTime = time.split(':');
        let meetingTime: MeetingTimes = new MeetingTimes(this.meeting,this.myForm.value.myDate.jsdate, 1, this.myForm.value.myDate.formatted);
        meetingTime.startDate.setHours(splittedTime[0], splittedTime[1]);
        console.log(meetingTime);
        this.meetingTimeService.addToMeetingTimes(meetingTime)
        .subscribe((response: Response)=>{;
          meetingTime.ID = response['ID'];
          this.meetingTimes.push(meetingTime);
        });
    }
    AddToMeetingParticipations(participant: any, meetingtime: any, status: string){
      //Assume email has been set correctly
      this.meetingParticipationsService.checkParticipationExistance(meetingtime.ID, participant.personID, participant.personalmail, status)
      this.checkIfParticipationsAvailable();
    }
    checkIfParticipationsAvailable(){
      var participantAmount = this.participants.length;
      let IDsAvailable = [];
      for (var i = 0; i < this.meetingTimes.length; i++){
        for (var j = i * participantAmount  ; j < (i * participantAmount) + (this.participants.length); j++){
          if (this.meetingParticipants[j].availability == "Beschikbaar"){
            IDsAvailable.push(this.meetingParticipants[j].meetingtimes);
          }
        }
        console.log(IDsAvailable)
        if (countInArray(IDsAvailable, this.meetingTimes[i].ID) == 3){
          this.planMeetingIn(i);
        }
      }
      function countInArray(array, what) {
        var count = 0;
        for (var i = 0; i < array.length; i++) {
          if (array[i] === what) {
            count++;
          }
        }
        return count;
      }
    }

    planMeetingIn(meeting: number){
      console.log(this.meetingTimes[meeting])
    }

    GetPersonByMail(email: string, exists?: boolean){
      if (email.toLowerCase().indexOf("ometa") != -1){
        this.participantService.getInternalParticipantFromEmail(email)
         .subscribe((odata)=>{
           let data = odata.value[0] ;
           let person = new Participant(data['First Name'] + " " + data['Last Name'], data['Email'], 0);
           if (this.participantEmails.indexOf(person.personalmail) == -1){
             this.participantEmails.push(person.personalmail);
             this.participants.push(person);
             if (!exists){
               for(var i = 0; i < this.meetingTimes.length; i++){
                 this.AddToMeetingParticipations(person, this.meetingTimes[i], "Misschien");
               }
             }
           }
        });
      }
      else{
        this.participantService.getExternalParticipantFromEmail(email)
          .subscribe((odata)=>{
          let data = odata.value[0] ;
          let person = new Participant(data['First Name DB'] + " " + data['Last Name DB'], data['EmailDB'], data['DBId']);
            if (this.participantEmails.indexOf(person.personalmail) == -1){
              this.participantEmails.push(person.personalmail);
              this.participants.push(person);
              if (!exists){
                for(var i = 0; i < this.meetingTimes.length; i++){
                  this.AddToMeetingParticipations(person, this.meetingTimes[i], "Misschien");
                }
              }
            }
        });
      }
    }



    //Supplimentary code for the datepicker
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
  };

  public myForm: FormGroup;

  setDate(): void {
    // Set today date using the patchValue function
    let date = new Date();
    this.myForm.patchValue({myDate: {
      date: {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate()}
    }});
  }

  clearDate(): void {
    // Clear the date using the patchValue function
    this.myForm.patchValue({myDate: null});
  }
}
