import {Routes, RouterModule} from "@angular/router";
import {MeetingPlannerComponent} from "./meetingPlanner/meeting-planner.component";
/**
 * Created by Delt on 19/02/2018.
 */

const APP_ROUTES: Routes = [
    {path: 'planner/:id', component: MeetingPlannerComponent},
    {path: 'planner/:id/:email', component: MeetingPlannerComponent},
];

export const routing = RouterModule.forRoot(APP_ROUTES);
